import { Route } from "react-router-dom"
import { BucketBawahan } from "../pages/BucketBawahan"
import { BucketDelegasi } from "../pages/BucketDelegasi"
import { BuckeNew } from "../pages/BucketNew"
import { BucketReschedule } from "../pages/BucketReschedule"
import { DashboardPage } from "../pages/DashboardPage"
import Login from "../pages/Login"
export const Routes=()=> {
    return(
        <>
        <Route path="/login" component={Login}/>
        <Route path="/dashboard" component={DashboardPage}/>
        <Route path="/bucket-new" component={BuckeNew}/>
        <Route path="/bucket-reschedule" component={BucketReschedule}/>
        <Route path="/bucket-delegasi" component={BucketDelegasi}/>
        <Route path="/bucket-bawahan" component={BucketBawahan}/>
        </>
    )
}