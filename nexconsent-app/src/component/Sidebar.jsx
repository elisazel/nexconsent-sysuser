import React from "react";
import { Link } from 'react-router-dom';
import { useLocation } from "react-router";
export const Sidebar = () => {  
  const location = useLocation();

  //destructuring pathname from location
  const { pathname } = location;

  //Javascript split method to get the name of the path in array
  const splitLocation = pathname.split("/");
    return (
        <>
            <div class="min-h-screen flex flex-row bg-gray-100">
                <div class="flex flex-col w-320 bg-white overflow-hidden">
                    <div class="flex w-252 items-center justify-center m-6">
                        <img src="assets/images/Nexconsent.svg" class="w-252 h-58 left-2 top-4 right-2" />
                    </div>
                    <ul class="flex flex-col py-4 m-5 dropdown">
                        <li>
                            <a class=" flex flex-row items-center h-12 transform hover:translate-x-2transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800" type="button" data-dropdown-toggle="dropdown">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/dashboard.svg" /></span>
                                <span class="text-sm font-medium">Dashboard</span>
                                <span class="m-4 w-4"><img src="assets/images/Icon DD.svg"/></span>
                            </a>
                            
                        </li>
                        <ul class="dropdown-menu hidden text-gray-700 pt-1">
                                <li class=""><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" href="#">Dashboard Bucket</a></li>
                                <li class=""><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" href="#">Dashboard Document</a></li>
                            </ul>
                        <li>
                            <a href="#" class="flex flex-row items-center h-12 transform hover:translate-x-2 transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800" type="button" data-dropdown-toggle="dropdown">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/list(4).svg" /></span>
                                <span class="text-sm font-medium ">Bucket</span>
                                <span class="m-4 w-4"><img src="assets/images/Icon DD.svg" /></span>
                            </a>
                        </li>
                        <ul class="dropdown-menu hidden text-gray-700 pt-1">
                                <li class=""><Link to="/bucket-new"><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" >Bucket New</a></Link></li>
                                <li class=""><Link to="/bucket-reschedule"><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" >Bucket Reschedule</a></Link></li>
                                <li class=""><Link to="/bucket-delegasi"><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" >Bucket Delegasi</a></Link></li>
                                <li class=""><Link to="/bucket-bawahan"><a class="rounded bg-white hover:bg-gray-200 py-2 px-4 block whitespace-no-wrap" >Bucket Bawahan</a></Link></li>
                            </ul>
                        <li>
                            <a href="#" class="flex flex-row items-center h-12 transform hover:translate-x-2 transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/history(2).svg" /></span>
                                <span class="text-sm font-medium">History Approval</span>
                                <span class="m-4 w-4"><img src="assets/images/Icon DD.svg" /></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flex flex-row items-center h-12 transform hover:translate-x-2 transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/dashboard.svg" /></span>
                                <span class="text-sm font-medium">Request Approval</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flex flex-row items-center h-12 transform hover:translate-x-2 transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/Tracking Icon.svg" /></span>
                                <span class="text-sm font-medium">Tracking Document</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flex flex-row items-center h-12 transform hover:translate-x-2 transition-transform ease-in duration-200 text-gray-500 hover:text-gray-800">
                                <span class="inline-flex items-center justify-center h-12 w-12 text-lg text-gray-400"><img src="assets/images/Report.svg" /></span>
                                <span class="text-sm font-medium">Report</span>
                                <span class="m-4 w-4"><img src="assets/images/Icon DD.svg" /></span>
                            </a>
                        </li>
                    </ul>
                </div>          
            </div>
        </>
    )
}