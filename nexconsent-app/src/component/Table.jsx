import React from "react";
import { Approve } from "./modals/Approve"

export const Table = () => {
    const [showModal, setShowModal] = React.useState(false);
    return (
        <>
            <div class="flex flex-col w-9/12 m-7 bg-center">
                <div class="container flex mx-auto">
                    <div class="flex border-2 bg-#E8E9EC rounded mb-4">
                        {/* <button class="flex items-center justify-center px-4 border-r">
                            <svg class="w-6 h-6 text-gray-600" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24">
                                <path
                                    d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z">
                                </path>
                            </svg>
                        </button> */}
                        <span class="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 bg-transparent rounded text-base items-center
      justify-center
      w-8
      pl-2
      py-1
    "
                        >
                            <i class="fas fa-search"></i>
                        </span>
                        <input type="text" class="px-4 py-2 w-80 h-8" placeholder="&nbsp; &nbsp; &nbsp;Search Customer Name, Rewards Name, ETC" />
                    </div>
                    <a type="button" class="ml-2" onClick={() => setShowModal(true)}><img src="assets/images/Rectangle 166 (2).svg" class="h-9" /></a>
                    <a type="button" class="ml-2" onClick={() => setShowModal(true)}><img src="assets/images/Rectangle 166 (4).svg" class="h-9" /></a>
                </div>
                {showModal ? (
                    <Approve />
                ) : null}

                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray">
                                    <tr>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            <img src="assets/images/checkbox outline.svg" class="w-4" />
                                        </th>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            Document ID
                                        </th>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            Tipe Approval
                                        </th>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            Requestor Name
                                        </th>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            Created Date
                                        </th>
                                        <th
                                            scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider"
                                        >
                                            Bucket Form</th>
                                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium md:font-bold  text-#424242 tracking-wider">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr class="bg-white divide-y divide-gray-200">
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"><img src="assets/images/checkbox outline.svg" class="w-4" /></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">test</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">test</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">test</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">test</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">test</div>
                                        </td>
                                        <td class="px-6 py-4">
                                            <a type="button" ><i><img src="assets/images/Check Green.svg" class="w-5" /></i></a>&nbsp;
                                            <a type="button" ><i><img src="assets/images/delete Red.svg" class="w-4" /></i></a>
                                        </td>
                                    </tr>
                                    <tr class="bg-gray divide-y divide-#00000029">
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-#808495">1 of 1</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900"></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}