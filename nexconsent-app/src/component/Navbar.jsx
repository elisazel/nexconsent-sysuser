import React from "react";
import { useLocation } from "react-router";

export const Navbar = () => {
    const [navbarOpen, setNavbarOpen] = React.useState(false);
    const location = useLocation();
    const { pathname } = location;
    const splitLocation = pathname.split("/");
    return (
        <>
            <nav class="relative flex flex-wrap items-center justify-between bg-white mb-3">
                <div class="container px-4 mx-auto flex flex-wrap items-center justify-between">
                    <div class="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                        <a
                            class="text-sm font-bold leading-relaxed inline-block py-2 whitespace-nowrap text-#424242"
                            href="#pablo"
                        >
                            {
                                location.pathname === "/bucket-new" ? "Bucket New" :
                                    location.pathname === "/bucket-reschedule" ? "Bucket Reschedule" :
                                        location.pathname === "/bucket-delegasi" ? "Bucket Delegasi" :
                                            location.pathname === "/bucket-bawahan" ? "Bucket Bawahan" : <></>
                            }
                        </a>
                        <button
                            class="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                            type="button"
                            onClick={() => setNavbarOpen(!navbarOpen)}
                        >
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                    <div
                        class={
                            "lg:flex flex-grow items-center" +
                            (navbarOpen ? " flex" : " hidden")
                        }
                        id="example-navbar-danger">
                        {/* <ul class="flex flex-col lg:flex-row list-none lg:ml-auto">
                            <li class="nav-item">
                                <a class="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-#424242 hover:opacity-75" href="#pablo">
                                    <span class="ml-2">Share</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-#424242 hover:opacity-75" href="#pablo">
                                    <span class="ml-2">Tweet</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-#424242 hover:opacity-75" href="#pablo">
                                    <span class="ml-2">Pin</span>
                                </a>
                            </li>
                        </ul> */}
                    </div>
                </div>
            </nav>
        </>
    )
}