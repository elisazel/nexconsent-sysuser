import { Navbar } from "../component/Navbar"
import { Sidebar } from "../component/Sidebar"
import { Table } from "../component/Table"

export const BucketReschedule = () => {
    return(
        <>
        <Navbar/>
        <div class="min-h-screen flex flex-row bg-gray-100">
        <Sidebar/>
        <Table/>
        </div>
        </>
    )
}